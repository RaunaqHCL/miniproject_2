package com.swayairapp.miniproject_2.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swayairapp.miniproject_2.entities.FlightEntity;
import com.swayairapp.miniproject_2.repository.FlightRepository;

@Service
public class FlightService {

    @Autowired
    FlightRepository repo;

    public FlightEntity addFlight(FlightEntity flight) {
        return repo.save(flight);
    }

    public String removeFlight(int flightId) {
        if (repo.existsById(flightId)) {
            repo.deleteById(flightId);
            return "Deleted successfully";
        } else {
            return "Resource doesn't exist";
        }
    }

    public FlightEntity updateFlight(int flightId, FlightEntity flight) {
        if (repo.existsById(flightId)) {
            flight.setFlightId(flightId);
            return repo.save(flight);
        } else {
            return null;
        }
    }

    public List<FlightEntity> getAllFlights() {
        return repo.findAll();
    }

    public FlightEntity getFlight(int flightId) {
        return repo.findById(flightId).orElse(null);
    }

    public List<FlightEntity> findByFilter(
            double minPrice, double maxPrice,
            Timestamp departureTime, Timestamp arrivalTime,
            int noOfStops, String airlines
    ) {
        return repo.findByPriceBetweenAndDepartureTimeGreaterThanEqualAndArrivalTimeLessThanEqualAndNoOfStopsAndAirlines(
                minPrice, maxPrice, departureTime, arrivalTime, noOfStops, airlines);
    }


   
    
}
