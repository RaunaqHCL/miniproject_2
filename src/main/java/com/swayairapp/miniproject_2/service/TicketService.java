package com.swayairapp.miniproject_2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swayairapp.miniproject_2.entities.TicketEntity;
import com.swayairapp.miniproject_2.repository.TicketRepository;

@Service
public class TicketService {

    @Autowired
    private TicketRepository ticketRepository;
    
    public List<TicketEntity> getAllTicketsByOneUser(int userId){
    	return ticketRepository.findByUserId(userId);
    }

    public TicketEntity bookTicket(TicketEntity ticket) {
        return ticketRepository.save(ticket);
    }

    public TicketEntity getTicketById(int ticketId) {
        return ticketRepository.findById(ticketId).orElse(null);
    }

    public void cancelTicket(int ticketId) {
        ticketRepository.deleteById(ticketId);
    }


}
