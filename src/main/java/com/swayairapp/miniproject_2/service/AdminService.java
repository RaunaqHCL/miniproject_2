package com.swayairapp.miniproject_2.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import com.swayairapp.miniproject_2.entities.RoleEntity;
import com.swayairapp.miniproject_2.entities.UserEntity;
import com.swayairapp.miniproject_2.repository.RoleRepository;
import com.swayairapp.miniproject_2.repository.UserRepository;


@Service
public class AdminService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    

    public UserEntity registerNewUser(UserEntity user) {
        RoleEntity role = roleRepo.findById("Admin").get();
        Set<RoleEntity> userRoles = new HashSet<>();
        userRoles.add(role);
        user.setRole(userRoles);
        user.setPassword(getEncodedPassword(user.getPassword()));

        return userRepo.save(user);
    }

    public String getEncodedPassword(String password) {
        return passwordEncoder.encode(password);
    }

    
}
