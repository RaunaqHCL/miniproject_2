package com.swayairapp.miniproject_2.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.swayairapp.miniproject_2.entities.RoleEntity;
import com.swayairapp.miniproject_2.entities.UserEntity;
import com.swayairapp.miniproject_2.repository.RoleRepository;
import com.swayairapp.miniproject_2.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void initRoleAndUser() {
    	

    	RoleEntity adminRole = new RoleEntity();
        adminRole.setRoleName("Admin");
        adminRole.setRoleDescription("Admin role");
        roleRepo.save(adminRole);

        RoleEntity userRole = new RoleEntity();
        userRole.setRoleName("User");
        userRole.setRoleDescription("Default role for newly created record");
        roleRepo.save(userRole);

        UserEntity adminUser = new UserEntity();
        adminUser.setUsername("admin123");
        adminUser.setPassword(getEncodedPassword("admin@pass"));
        adminUser.setName("admin");
        Set<RoleEntity> adminRoles = new HashSet<>();
        adminRoles.add(adminRole);
        adminUser.setRole(adminRoles);
        userRepo.save(adminUser);

        UserEntity user = new UserEntity();
        user.setUsername("AlphaRaunaq");
        user.setPassword(getEncodedPassword("raunaq@123"));
        user.setName("Raunaq");
        Set<RoleEntity> userRoles = new HashSet<>();
        userRoles.add(userRole);
        user.setRole(userRoles);
        userRepo.save(user);
    }
    
    public UserEntity getUserById(int userId) {
    	//TODO WRAP IN TRY CATCH AND CREATE CUSTOM EXCEPTION
    	UserEntity user = null;
    	
    	user = userRepo.getById(userId);
    	
    	return user;
    	
    }

    public UserEntity registerNewUser(UserEntity user) {
        RoleEntity role = roleRepo.findById("User").get();
        Set<RoleEntity> userRoles = new HashSet<>();
        userRoles.add(role);
        user.setRole(userRoles);
        user.setPassword(getEncodedPassword(user.getPassword()));

        return userRepo.save(user);
    }

    public String getEncodedPassword(String password) {
        return passwordEncoder.encode(password);
    }
}