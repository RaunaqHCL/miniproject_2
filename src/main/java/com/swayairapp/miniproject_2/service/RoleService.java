package com.swayairapp.miniproject_2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swayairapp.miniproject_2.entities.RoleEntity;
import com.swayairapp.miniproject_2.repository.RoleRepository;

@Service
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepo;
	
	public RoleEntity createNewRole(RoleEntity role) {
		return roleRepo.save(role);
	}

}
