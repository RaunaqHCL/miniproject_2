package com.swayairapp.miniproject_2.controller;

import com.swayairapp.miniproject_2.entities.UserEntity;
import com.swayairapp.miniproject_2.entities.FlightEntity;
import com.swayairapp.miniproject_2.entities.SeatEntity;
import com.swayairapp.miniproject_2.entities.TicketEntity;
import com.swayairapp.miniproject_2.service.FlightService;
import com.swayairapp.miniproject_2.service.TicketService;
import com.swayairapp.miniproject_2.service.UserService;
import com.swayairapp.miniproject_2.util.JwtUtil;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private FlightService flightService;
    
    @Autowired
    private TicketService ticketService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    SeatEntity seat;
    
    @PostConstruct
    public void initRoleAndUser() {
        userService.initRoleAndUser();
    }

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody UserEntity userEntity) {
        // Encode the password before saving
        String encodedPassword = passwordEncoder.encode(userEntity.getPassword());
        userEntity.setPassword(encodedPassword);

        UserEntity savedUser = userService.registerNewUser(userEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body("User registered successfully");
    }

    @PostMapping("/login")
    public ResponseEntity<String> loginUser(@RequestBody UserEntity userEntity) {
        // Dummy authentication logic, replace with your own authentication logic
        if ("user".equals(userEntity.getUsername()) && "password".equals(userEntity.getPassword())) {
            // Generate JWT token
            String token = jwtUtil.generateToken(User.withUsername(userEntity.getUsername()).password(userEntity.getPassword()).roles("User").build());
            return ResponseEntity.ok().body(token);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid username or password");
        }
    }
    
    @GetMapping("/flights")
    public ResponseEntity<List<FlightEntity>> getAllFlights(){
    	//TODO ADD CUSTOM EXCEPTION IF FLIGHTS NOT FOUND
    	List<FlightEntity> flights = null;
    	
    	flights = flightService.getAllFlights();
    	
    	if(flights.size() == 0) {
    		return ResponseEntity.notFound().build();
    	}
    	
    	return ResponseEntity.ok().body(flights);
    	
    }
    
    @PostMapping("/ticket")
    public ResponseEntity<String> bookTicket(@RequestBody Map<String, Integer> requestBody){
    	Integer userId = requestBody.get("userId");
    	Integer flightId = requestBody.get("flightId");
    	Integer noOfSeats = requestBody.get("noOfSeats");
    	
    	if(userId == null || flightId == null || noOfSeats == null) {
    		return ResponseEntity.badRequest().body("Request body should contain userId, flightId and noOfSeats");
    	}
    	
    	UserEntity user = null;
    	user = userService.getUserById(userId);
    	
    	if(user == null) {
    		return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
    	}
    	
    	FlightEntity flight = null;
    	flight = flightService.getFlight(flightId);
    	if(flight == null) {
    		 return new ResponseEntity<>("Flight not found", HttpStatus.NOT_FOUND);
    	}

    	
    	if(flight.getNoOfSeats() < noOfSeats) {
    		return ResponseEntity.badRequest().body("Not enough available seats");
    	}
    	
    	List<String> seatsBooked = null;
    	
    	for(int i=0;i<noOfSeats;i++) {
    		seatsBooked.add(seat.generateRandomSeat());
    	}
    	
    	TicketEntity ticket = new TicketEntity();
    	ticket.setFlight(flight);
    	ticket.setUser(user);
    	ticket.setSeats(seatsBooked);
    	ticketService.bookTicket(ticket);
    	
    	flight.setNoOfSeats(flight.getNoOfSeats() - noOfSeats);
    	flightService.updateFlight(flightId, flight);
    	
    	return ResponseEntity.ok().body("Ticket Booked Successfully");
    }
    
    @GetMapping("/tickets/{userId}")
    public ResponseEntity<List<TicketEntity>> getAllTicketsByUserId(@PathVariable Integer userId){
    	if(userId == null) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	List<TicketEntity> tickets = null;
    	tickets = ticketService.getAllTicketsByOneUser(userId);
    	
    	if(tickets.isEmpty()) {
    		return ResponseEntity.notFound().build();
    		
    	}
    	
    	return ResponseEntity.ok().body(tickets);
    	
    }
    
    @DeleteMapping("/ticket/{ticketId}")
    public ResponseEntity<?> cancelTicket(@PathVariable Integer ticketId){
    	if(ticketId == null) {
    		return ResponseEntity.badRequest().body("Please enter ticketId");
    	}
    	
    	TicketEntity ticket = null;
    	ticket = ticketService.getTicketById(ticketId);
    	
    	if(ticket == null) {
    		return new ResponseEntity<>("Ticket Not Found", HttpStatus.NOT_FOUND);
    	}
    	
    	return ResponseEntity.ok().body(ticket);
    }

    @GetMapping("/flights")
    public ResponseEntity<?> getFlightsByFilter(
            @RequestParam(value = "minPrice") Double minPrice,
            @RequestParam(value = "maxPrice") Double maxPrice,
            @RequestParam(value = "departureTime") Timestamp departureTime,
            @RequestParam(value = "arrivalTime") Timestamp arrivalTime,
            @RequestParam(value = "noOfStops") Integer noOfStops,
            @RequestParam(value = "airlines") String airlines
    ) {
    	
    	if (minPrice == null && maxPrice == null && departureTime == null && arrivalTime == null && noOfStops == null && airlines.isEmpty()) {
            return ResponseEntity.badRequest().body("At least one filter parameter is required");
        }

        if (minPrice == null) {
            minPrice = 0.0;
        }
        if (maxPrice == null) {
            maxPrice = Double.MAX_VALUE;
        }
        if (departureTime == null) {
            departureTime = new Timestamp(0);
        }
        if (arrivalTime == null) {
            arrivalTime = new Timestamp(Long.MAX_VALUE);
        }
        if (noOfStops == null) {
            noOfStops = 0;
        }
        if (airlines.isEmpty()) {
            airlines = "";
        }
        List<FlightEntity> flights = flightService.findByFilter(minPrice, maxPrice, departureTime, arrivalTime, noOfStops, airlines);
        if (flights.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(flights);
    }
}
