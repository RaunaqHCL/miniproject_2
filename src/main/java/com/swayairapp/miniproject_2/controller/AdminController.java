package com.swayairapp.miniproject_2.controller;



import com.swayairapp.miniproject_2.entities.FlightEntity;
import com.swayairapp.miniproject_2.entities.UserEntity;
import com.swayairapp.miniproject_2.service.AdminService;
import com.swayairapp.miniproject_2.service.FlightService;
import com.swayairapp.miniproject_2.util.JwtUtil;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private FlightService flightService;
    
    @Autowired
    private AdminService adminService;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private JwtUtil jwtUtil;

    
    @PostMapping("/register")
    public ResponseEntity<String> registerAdmin(@RequestBody UserEntity userEntity) {
        // Encode the password before saving
        String encodedPassword = passwordEncoder.encode(userEntity.getPassword());
        userEntity.setPassword(encodedPassword);

        UserEntity savedUser = adminService.registerNewUser(userEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body("Admin registered successfully");
    }

    @PostMapping("/login")
    public ResponseEntity<String> loginUser(@RequestBody UserEntity userEntity) {
        
        if ("user".equals(userEntity.getUsername()) && "password".equals(userEntity.getPassword())) {
            String token = jwtUtil.generateToken(User.withUsername(userEntity.getUsername()).password(userEntity.getPassword()).roles("Admin").build());
            return ResponseEntity.ok().body(token);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid username or password");
        }
    }
    
    @GetMapping("/flights")
    public ResponseEntity<List<FlightEntity>> getAllFlights(){
    	//TODO ADD CUSTOM EXCEPTION IF FLIGHTS NOT FOUND
    	List<FlightEntity> flights = null;
    	
    	flights = flightService.getAllFlights();
    	
    	if(flights.size() == 0) {
    		return ResponseEntity.notFound().build();
    	}
    	
    	return ResponseEntity.ok().body(flights);
    	
    }
    @PostMapping("/flights")
    public FlightEntity addFlight(@RequestBody FlightEntity flight) {
    	//TODO ADD CUSTOM EXCEPTION
        return flightService.addFlight(flight);
    }

    @DeleteMapping("/flights/{flightId}")
    public String removeFlight(@PathVariable int flightId) {
    	//TODO ADD CUSTOM EXCEPTION
        return flightService.removeFlight(flightId);
    }

    @PutMapping("/flights/{flightId}")
    public FlightEntity updateFlight(@PathVariable int flightId, @RequestBody FlightEntity flight) {
    	//TODO ADD CUSTOM EXCEPTION
        return flightService.updateFlight(flightId, flight);
    }
}

