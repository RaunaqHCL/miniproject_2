package com.swayairapp.miniproject_2.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swayairapp.miniproject_2.entities.MessageEntity;
import com.swayairapp.miniproject_2.repository.MessageRepository;

@RestController
@RequestMapping("/messages")
public class MessageController {

	@Autowired
	private MessageRepository messageRepository;

	@GetMapping
	public List<MessageEntity> getAllMessages() {
		return messageRepository.findAll();
	}

	@PostMapping
	public MessageEntity sendMessage(@RequestBody MessageEntity message) {

		message.setTimestamp(LocalDateTime.now());
		return messageRepository.save(message);
	}
};
