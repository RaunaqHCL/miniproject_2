package com.swayairapp.miniproject_2.repository;



import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.swayairapp.miniproject_2.entities.UserEntity;


@Repository
public interface UserRepository extends JpaRepository<UserEntity,Integer>{
	Optional<UserEntity>findByUsername(String username);
}
