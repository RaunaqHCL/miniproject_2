package com.swayairapp.miniproject_2.repository;

import com.swayairapp.miniproject_2.entities.FlightEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<FlightEntity, Integer> {

	List<FlightEntity> findByPriceBetweenAndDepartureTimeGreaterThanEqualAndArrivalTimeLessThanEqualAndNoOfStopsAndAirlines(
	        double minPrice, double maxPrice,
	        Timestamp departureTime, Timestamp arrivalTime,
	        int noOfStops, String airlines
	    );
}

