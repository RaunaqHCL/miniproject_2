package com.swayairapp.miniproject_2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.swayairapp.miniproject_2.entities.TicketEntity;

public interface TicketRepository extends JpaRepository<TicketEntity, Integer> {
	 List<TicketEntity> findByUserId(int userId);
}

