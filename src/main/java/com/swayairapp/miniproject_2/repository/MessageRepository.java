package com.swayairapp.miniproject_2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.swayairapp.miniproject_2.entities.MessageEntity;


public interface MessageRepository extends JpaRepository<MessageEntity, Long> {
    
}
