package com.swayairapp.miniproject_2.entities;

import java.util.Random;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="SeatTable")
public class SeatEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="seat_id")
	private int seatId;
	@Column(name="seat_no", unique= true)
	private String seatNo;
	@Column(name="Status")
	private String status;
	
	@ManyToOne
	private TicketEntity ticket;
	
	public SeatEntity() {
		super();
		this.seatNo = generateRandomSeat();
	}
	
	public SeatEntity(String status) {
		super();
		this.seatNo = generateRandomSeat();
		this.status = status;
	}
	
	public static String generateRandomSeat() {
		final String[] ROWS = {"A", "B", "C", "D", "E", "F"};
		final int MAX_ROWS = ROWS.length;
		final int MAX_COLS = 6;
		Random random = new Random();
		int row = random.nextInt(MAX_ROWS);
		int col = random.nextInt(MAX_COLS) + 1;
		return ROWS[row] + col;
		
	}

	public int getSeatId() {
		return seatId;
	}

	public String getSeatNo() {
		return seatNo;
	}

	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public TicketEntity getTicket() {
        return ticket;
    }

    public void setTicket(TicketEntity ticket) {
        this.ticket = ticket;
    }

	@Override
	public String toString() {
		return "SeatEntity [seatId=" + seatId + ", seatNo=" + seatNo + ", status=" + status + "]";
	}

}
