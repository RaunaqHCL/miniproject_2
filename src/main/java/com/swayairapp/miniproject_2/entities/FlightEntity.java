package com.swayairapp.miniproject_2.entities;

import java.sql.Timestamp;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="FlightDetails")
public class FlightEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int flightId;
	
	@Column(name="Duration")
	private String duration;
	@Column(name="Destination")
	private String destination;
	@Column(name="Source")
	private String source;
	@Column(name="Price")
	private double price;
	@Column(name="NoOfStops")
	private int noOfStops;
	@Column(name="NoOfSeats")
	private int noOfSeats;
	@Column(name="Airlines")
	private String airlines;
	@Column(name="DepartureTime")
	private Timestamp departureTime;
	@Column(name="ArrivalTime")
	private Timestamp arrivalTime;
	
	@OneToMany(mappedBy = "flight")
    private Set<TicketEntity> tickets;
	
	public FlightEntity() {
		super();
		
	}

	public FlightEntity(String duration, String destination, String source, double price, int noOfStops,
			int noOfSeats, String airlines, Timestamp departureTime, Timestamp arrivalTime) {
		super();
		this.duration = duration;
		this.destination = destination;
		this.source = source;
		this.price = price;
		this.noOfStops = noOfStops;
		this.noOfSeats = noOfSeats;
		this.airlines = airlines;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getNoOfStops() {
		return noOfStops;
	}

	public void setNoOfStops(int noOfStops) {
		this.noOfStops = noOfStops;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	public String getAirlines() {
		return airlines;
	}

	public void setAirlines(String airlines) {
		this.airlines = airlines;
	}

	public Timestamp getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Timestamp departureTime) {
		this.departureTime = departureTime;
	}

	public Timestamp getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Timestamp arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	@Override
	public String toString() {
		return "FlightEntity [flightId=" + flightId + ", duration=" + duration + ", destination=" + destination
				+ ", source=" + source + ", price=" + price + ", noOfStops=" + noOfStops + ", noOfSeats=" + noOfSeats
				+ ", airlines=" + airlines + ", departureTime=" + departureTime + ", arrivalTime=" + arrivalTime + "]";
	}
	
	
	
	

}
