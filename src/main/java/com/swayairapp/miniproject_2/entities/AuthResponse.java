package com.swayairapp.miniproject_2.entities;



public class AuthResponse {

    private UserEntity user;
    private String token;

    public AuthResponse(UserEntity user, String token) {
        this.user = user;
        this.token = token;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getJwtToken() {
        return token;
    }

    public void setJwtToken(String token) {
        this.token = token;
    }
}
