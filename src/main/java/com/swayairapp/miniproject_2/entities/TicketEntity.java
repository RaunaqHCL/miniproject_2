package com.swayairapp.miniproject_2.entities;

import java.util.List;
import java.util.Set;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="TicketTable")
public class TicketEntity {
	
	@Id
	@Column(name="TICKET_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int ticketId;
	@ManyToOne
	@JoinColumn(name="user_id")
	private UserEntity user;
	
	@ManyToOne
	@JoinColumn(name="flight_id")
	private FlightEntity flight;
	
	@Column(name="Seats")
	private List<String> seats;
	
	@Column(name="BookingStatus")
	private String bookingStatus;
	
	public TicketEntity() {
		super();
		
	}
	
	public TicketEntity(String bookingStatus) {
		super();
		this.bookingStatus = bookingStatus;
	}

	public int getTicketId() {
		return ticketId;
	}


	public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public FlightEntity getFlight() {
        return flight;
    }

    public void setFlight(FlightEntity flight) {
        this.flight = flight;
    }

    public List<String> getSeats() {
        return seats;
    }

    public void setSeats(List<String> seats) {
        this.seats = seats;
    }

	@Override
	public String toString() {
		return "TicketEntity [ticketId=" + ticketId + ", user=" + user + ", flight=" + flight + ", seats=" + seats
				+ ", bookingStatus=" + bookingStatus + "]";
	}

	
}
