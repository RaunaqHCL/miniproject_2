package com.swayairapp.miniproject_2.servicetest;



import com.swayairapp.miniproject_2.entities.RoleEntity;
import com.swayairapp.miniproject_2.entities.UserEntity;
import com.swayairapp.miniproject_2.repository.RoleRepository;
import com.swayairapp.miniproject_2.repository.UserRepository;
import com.swayairapp.miniproject_2.service.UserService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;

    @Test
    public void initRoleAndUserTest() {
        RoleEntity adminRole = new RoleEntity();
        adminRole.setRoleName("Admin");
        adminRole.setRoleDescription("Admin role");

        RoleEntity userRole = new RoleEntity();
        userRole.setRoleName("User");
        userRole.setRoleDescription("Default role for newly created record");

        UserEntity adminUser = new UserEntity();
        adminUser.setUsername("admin123");
        adminUser.setPassword("encodedPassword");
        adminUser.setName("admin");

        UserEntity user = new UserEntity();
        user.setUsername("AlphaRaunaq");
        user.setPassword("encodedPassword");
        user.setName("Raunaq");

        when(roleRepository.findById("Admin")).thenReturn(Optional.of(adminRole));
        when(roleRepository.findById("User")).thenReturn(Optional.of(userRole));
        when(passwordEncoder.encode("admin@pass")).thenReturn("encodedPassword");
        when(passwordEncoder.encode("raunaq@123")).thenReturn("encodedPassword");

        userService.initRoleAndUser();

        verify(roleRepository, times(2)).save(any(RoleEntity.class));
        verify(userRepository, times(2)).save(any(UserEntity.class));
    }

    @Test
    public void registerNewUserTest() {
        RoleEntity userRole = new RoleEntity();
        userRole.setRoleName("User");
        userRole.setRoleDescription("Default role for newly created record");

        UserEntity user = new UserEntity();
        user.setUsername("JohnDoe");
        user.setPassword("password");
        user.setName("John Doe");

        when(roleRepository.findById("User")).thenReturn(Optional.of(userRole));
        when(passwordEncoder.encode("password")).thenReturn("encodedPassword");
        when(userRepository.save(user)).thenReturn(user);

        UserEntity savedUser = userService.registerNewUser(user);

        assertEquals("encodedPassword", savedUser.getPassword());
        assertEquals(1, savedUser.getRole().size());
    }

  
}

