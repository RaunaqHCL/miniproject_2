package com.swayairapp.miniproject_2.servicetest;

import com.swayairapp.miniproject_2.entities.FlightEntity;
import com.swayairapp.miniproject_2.entities.TicketEntity;
import com.swayairapp.miniproject_2.entities.UserEntity;
import com.swayairapp.miniproject_2.repository.TicketRepository;
import com.swayairapp.miniproject_2.service.TicketService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TicketServiceTest {

	@Mock
	private TicketRepository ticketRepository;

	@InjectMocks
	private TicketService ticketService;

	@Test
	public void getAllTicketsByOneUserTest() {
		int userId = 1;
		List<TicketEntity> expectedTickets = new ArrayList<>();
		when(ticketRepository.findByUserId(userId)).thenReturn(expectedTickets);
		List<TicketEntity> result = ticketService.getAllTicketsByOneUser(userId);
		assertEquals(expectedTickets, result);
	}

	@Test
	public void bookTicketTest() {
		TicketEntity ticket = new TicketEntity();
		ticket.setBookingStatus("Booked");
		when(ticketRepository.save(ticket)).thenReturn(ticket);
		TicketEntity result = ticketService.bookTicket(ticket);
		assertEquals(ticket, result);
	}

	@Test
	public void getTicketByIdTest() {
		int ticketId = 1;
		TicketEntity expectedTicket = new TicketEntity();
		when(ticketRepository.findById(ticketId)).thenReturn(java.util.Optional.of(expectedTicket));
		TicketEntity result = ticketService.getTicketById(ticketId);
		assertEquals(expectedTicket, result);
	}

	@Test
	public void cancelTicketTest() {
		int ticketId = 1;
		ticketService.cancelTicket(ticketId);
	}
}
