package com.swayairapp.miniproject_2.servicetest;

import com.swayairapp.miniproject_2.entities.RoleEntity;
import com.swayairapp.miniproject_2.entities.UserEntity;
import com.swayairapp.miniproject_2.repository.RoleRepository;
import com.swayairapp.miniproject_2.repository.UserRepository;
import com.swayairapp.miniproject_2.service.AdminService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AdminServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AdminService adminService;

    @Test
    public void registerNewUserTest() {
        UserEntity user = new UserEntity("John Doe", "john@example.com", "password", "john_doe");
        RoleEntity role = new RoleEntity();
        role.setRoleName("Admin");
        Set<RoleEntity> roles = new HashSet<>();
        roles.add(role);

        when(roleRepository.findById("Admin")).thenReturn(Optional.of(role));
        when(passwordEncoder.encode(user.getPassword())).thenReturn("encodedPassword");
        when(userRepository.save(user)).thenReturn(user);

        UserEntity savedUser = adminService.registerNewUser(user);

        assertEquals(1, savedUser.getRole().size());
        assertEquals("encodedPassword", savedUser.getPassword());
    }
}


