package com.swayairapp.miniproject_2.servicetest;

import com.swayairapp.miniproject_2.entities.RoleEntity;
import com.swayairapp.miniproject_2.repository.RoleRepository;
import com.swayairapp.miniproject_2.service.RoleService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTest {

	@Mock
	private RoleRepository roleRepository;

	@InjectMocks
	private RoleService roleService;

	@Test
	public void createNewRoleTest() {
		RoleEntity role = new RoleEntity();
		role.setRoleName("Admin");
		role.setRoleDescription("Admin role");
		when(roleRepository.save(role)).thenReturn(role);
		RoleEntity result = roleService.createNewRole(role);
		assertEquals(role, result);
	}
}
