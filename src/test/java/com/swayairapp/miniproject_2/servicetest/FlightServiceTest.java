package com.swayairapp.miniproject_2.servicetest;

import com.swayairapp.miniproject_2.entities.FlightEntity;
import com.swayairapp.miniproject_2.repository.FlightRepository;
import com.swayairapp.miniproject_2.service.FlightService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {

	@Mock
	private FlightRepository flightRepository;

	@InjectMocks
	private FlightService flightService;

	@Test
	public void addFlightTest() {
		// Mock data
		FlightEntity flight = new FlightEntity();
		flight.setDuration("2 hours");
		flight.setSource("New York");
		flight.setDestination("London");
		flight.setPrice(500.0);
		flight.setNoOfStops(1);
		flight.setNoOfSeats(200);
		flight.setAirlines("SwayAir");
		flight.setDepartureTime(new Timestamp(System.currentTimeMillis()));
		flight.setArrivalTime(new Timestamp(System.currentTimeMillis() + 7200000));

		FlightEntity result = flightService.addFlight(flight);

		assertEquals(flight, result);
	}

	@Test
	public void removeFlightTest() {

		int flightId = 1;

		when(flightRepository.existsById(flightId)).thenReturn(true);
		String result = flightService.removeFlight(flightId);
		assertEquals("Deleted successfully", result);
		verify(flightRepository, times(1)).deleteById(flightId);
	}

	@Test
	public void updateFlightTest() {
		int flightId = 1;
		FlightEntity flight = new FlightEntity();
		flight.setDuration("2 hours");
		flight.setSource("New York");
		flight.setDestination("London");
		flight.setPrice(500.0);
		flight.setNoOfStops(1);
		flight.setNoOfSeats(200);
		flight.setAirlines("SwayAir");
		flight.setDepartureTime(new Timestamp(System.currentTimeMillis()));
		flight.setArrivalTime(new Timestamp(System.currentTimeMillis() + 7200000)); // 2 hours later

		when(flightRepository.existsById(flightId)).thenReturn(true);
		when(flightRepository.save(flight)).thenReturn(flight);

		FlightEntity result = flightService.updateFlight(flightId, flight);

		assertEquals(flight, result);
	}

	@Test
	public void getAllFlightsTest() {

		List<FlightEntity> flights = new ArrayList<>();
		when(flightRepository.findAll()).thenReturn(flights);
		List<FlightEntity> result = flightService.getAllFlights();
		assertEquals(flights, result);
	}

	@Test
	public void getFlightTest() {
		int flightId = 1;
		FlightEntity flight = new FlightEntity();
		flight.setDuration("2 hours");
		flight.setSource("New York");
		flight.setDestination("London");
		flight.setPrice(500.0);
		flight.setNoOfStops(1);
		flight.setNoOfSeats(200);
		flight.setAirlines("SwayAir");
		flight.setDepartureTime(new Timestamp(System.currentTimeMillis()));
		flight.setArrivalTime(new Timestamp(System.currentTimeMillis() + 7200000)); // 2 hours later
		when(flightRepository.findById(flightId)).thenReturn(Optional.of(flight));
		FlightEntity result = flightService.getFlight(flightId);
		assertEquals(flight, result);
	}

}
