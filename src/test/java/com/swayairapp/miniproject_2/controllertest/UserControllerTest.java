package com.swayairapp.miniproject_2.controllertest;



import com.fasterxml.jackson.databind.ObjectMapper;
import com.swayairapp.miniproject_2.controller.UserController;
import com.swayairapp.miniproject_2.entities.UserEntity;
import com.swayairapp.miniproject_2.exceptions.UserNotFoundException;
import com.swayairapp.miniproject_2.service.FlightService;
import com.swayairapp.miniproject_2.service.TicketService;
import com.swayairapp.miniproject_2.service.UserService;
import com.swayairapp.miniproject_2.util.JwtUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.Collections;
import java.util.Map;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private UserService userService;

    @Mock
    private FlightService flightService;

    @Mock
    private TicketService ticketService;

    @Mock
    private JwtUtil jwtUtil;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private UserController userController;

    @Test
    public void registerUserTest() throws Exception {
        UserEntity user = new UserEntity();
        user.setUsername("user");
        user.setPassword("password");
        user.setEmail("user@example.com");
        user.setName("User");
        when(userService.registerNewUser(user)).thenReturn(user);
        mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

//    @Test
//    public void loginUserTest() throws Exception {
//        UserEntity user = new UserEntity();
//        user.setUsername("user");
//        user.setPassword("password");
//
//        when(userService.getUserByUsernameAndPassword("user", "password")).thenReturn(user);
//
//        mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
//                .content(objectMapper.writeValueAsString(user))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk());
//    }

    @Test
    public void getUserByIdTest() throws Exception {
        int userId = 1;
        UserEntity user = new UserEntity();
        user.setId(userId);
        user.setUsername("user");
        user.setPassword("password");
        user.setEmail("user@example.com");
        user.setName("User");
        when(userService.getUserById(userId)).thenReturn(user);
        mockMvc.perform(MockMvcRequestBuilders.get("/user/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void bookTicketTest() throws Exception {
        UserEntity user = new UserEntity();
        user.setId(1);
        user.setUsername("user");
        user.setPassword("password");
        user.setEmail("user@example.com");
        user.setName("User");
        when(userService.getUserById(1)).thenReturn(user);
        Map<String, Integer> requestBody = Collections.singletonMap("userId", 1);
        requestBody.put("flightId", 1);
        requestBody.put("noOfSeats", 1);
        mockMvc.perform(MockMvcRequestBuilders.post("/user/ticket")
                .content(objectMapper.writeValueAsString(requestBody))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllTicketsByUserIdTest() throws Exception {
        int userId = 1;

        when(ticketService.getAllTicketsByOneUser(userId)).thenReturn(Collections.emptyList());
        mockMvc.perform(MockMvcRequestBuilders.get("/user/tickets/{userId}", userId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void cancelTicketTest() throws Exception {
        int ticketId = 1;

        when(ticketService.getTicketById(ticketId)).thenReturn(null);
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/ticket/{ticketId}", ticketId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}

