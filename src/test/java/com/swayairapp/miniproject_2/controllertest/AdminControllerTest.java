package com.swayairapp.miniproject_2.controllertest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swayairapp.miniproject_2.entities.FlightEntity;
import com.swayairapp.miniproject_2.service.FlightService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AdminControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private FlightService flightService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void addFlightTest() throws Exception {
        FlightEntity flight = new FlightEntity();
        flight.setFlightId(1);
        flight.setDuration("2 hours");
        flight.setDestination("Destination");
        flight.setSource("Source");
        flight.setPrice(100.0);
        flight.setNoOfStops(1);
        flight.setNoOfSeats(100);
        flight.setAirlines("Test Airlines");
        flight.setDepartureTime(new Timestamp(new Date().getTime()));
        flight.setArrivalTime(new Timestamp(new Date().getTime()));

        when(flightService.addFlight(flight)).thenReturn(flight);

        mockMvc.perform(MockMvcRequestBuilders.post("/admin/flights")
                .content(objectMapper.writeValueAsString(flight))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void removeFlightTest() throws Exception {
        int flightId = 1;

        when(flightService.removeFlight(flightId)).thenReturn("Flight removed successfully");

        mockMvc.perform(MockMvcRequestBuilders.delete("/admin/flights/{flightId}", flightId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateFlightTest() throws Exception {
        int flightId = 1;
        FlightEntity flight = new FlightEntity();
        flight.setFlightId(1);
        flight.setDuration("2 hours");
        flight.setDestination("Destination");
        flight.setSource("Source");
        flight.setPrice(100.0);
        flight.setNoOfStops(1);
        flight.setNoOfSeats(100);
        flight.setAirlines("Test Airlines");
        flight.setDepartureTime(new Timestamp(new Date().getTime()));
        flight.setArrivalTime(new Timestamp(new Date().getTime()));

        when(flightService.updateFlight(flightId, flight)).thenReturn(flight);

        mockMvc.perform(MockMvcRequestBuilders.put("/admin/flights/{flightId}", flightId)
                .content(objectMapper.writeValueAsString(flight))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}


