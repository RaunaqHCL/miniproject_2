package com.swayairapp.miniproject_2.repotest;

import com.swayairapp.miniproject_2.entities.RoleEntity;
import com.swayairapp.miniproject_2.repository.RoleRepository;
import com.swayairapp.miniproject_2.service.RoleService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RoleRepositoryTest {

	@Mock
	private RoleRepository roleRepository;

	@InjectMocks
	private RoleService roleService;

	@Test
	public void findAllTest() {

		List<RoleEntity> roles = new ArrayList<>();

		when(roleRepository.findAll()).thenReturn(roles);

		List<RoleEntity> result = roleRepository.findAll();

		assertEquals(roles, result);
	}

}
