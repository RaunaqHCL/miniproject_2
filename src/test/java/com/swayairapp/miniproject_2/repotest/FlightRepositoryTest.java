package com.swayairapp.miniproject_2.repotest;

import com.swayairapp.miniproject_2.entities.FlightEntity;
import com.swayairapp.miniproject_2.repository.FlightRepository;
import com.swayairapp.miniproject_2.service.FlightService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightRepositoryTest {

	@Mock
	private FlightRepository flightRepository;

	@InjectMocks
	private FlightService flightService;

	@Test
	public void findByFiltersTest() {
		double minPrice = 100.0;
		double maxPrice = 200.0;
		Timestamp departureTime = Timestamp.valueOf("2024-03-30 08:00:00");
		Timestamp arrivalTime = Timestamp.valueOf("2024-03-30 20:00:00");
		int noOfStops = 1;
		String airlines = "Test Airlines";

		List<FlightEntity> flights = new ArrayList<>();

		when(flightRepository
				.findByPriceBetweenAndDepartureTimeGreaterThanEqualAndArrivalTimeLessThanEqualAndNoOfStopsAndAirlines(
						minPrice, maxPrice, departureTime, arrivalTime, noOfStops, airlines))
				.thenReturn(flights);

		List<FlightEntity> result = flightRepository
				.findByPriceBetweenAndDepartureTimeGreaterThanEqualAndArrivalTimeLessThanEqualAndNoOfStopsAndAirlines(
						minPrice, maxPrice, departureTime, arrivalTime, noOfStops, airlines);

		assertEquals(flights, result);
	}

}
