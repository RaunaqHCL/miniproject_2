package com.swayairapp.miniproject_2.repotest;

import com.swayairapp.miniproject_2.entities.TicketEntity;
import com.swayairapp.miniproject_2.repository.TicketRepository;
import com.swayairapp.miniproject_2.service.TicketService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TicketRepositoryTest {

	@Mock
	private TicketRepository ticketRepository;

	@InjectMocks
	private TicketService ticketService;

	@Test
	public void findByUserIdTest() {

		int userId = 1;
		List<TicketEntity> tickets = new ArrayList<>();

		when(ticketRepository.findByUserId(userId)).thenReturn(tickets);

		List<TicketEntity> result = ticketRepository.findByUserId(userId);

		assertEquals(tickets, result);
	}

}
